from flask import Flask, render_template, jsonify, request
from flask_cors import CORS
import mysql.connector as sq                                             #Can be changed based on destination database stack
import json

app = Flask(__name__)
CORS(app)

#Obtain connection string information from the portal
config = {                                                               #Can be changed based on destination database stack
  'host':'34.69.124.125',
  'user':'admin',
  'password':'LiveDB#321',
  'database':'DM_Products'
}

#Initiate variables
success = {"success code": str(1),}
failure = {"success code": str(0),}

def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""
    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    if isinstance(obj, decimal.Decimal):
        return float(obj)
    raise TypeError ("Type %s not serializable" % type(obj))

#To check is the service is running
@app.route('/')
def home():
    """To check is the service is running"""
    return "<h1>Serivce is Up</h1>",200

@app.route('/setup')
def setup():
    """To be called if this is a fresh installation"""
    conn = sq.connect(**config)
    cur = conn.cursor()
    try:
        query = "CREATE TABLE MasterTable (ID int(11) NOT NULL AUTO_INCREMENT, TableName longtext, ViewName longtext, DisplayName longtext, KeyName longtext, CreatedByID int(11) DEFAULT '0', CreatedOn timestamp NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (ID))"
        cur.execute(query)
        
        query = "CREATE VIEW MasterTableVw AS SELECT DisplayName, KeyName FROM MasterTable"
        cur.execute(query)
		
		query = "INSERT INTO MasterTable ( TableName, ViewName, DisplayName, KeyName) VALUES ('MasterTable', 'MasterTableVw', 'ConfigTable', 'ID')"
        cur.execute(query)
        
        return jsonify(success)
    except Exception as e:
        failure['error message'] = str(e)
        return jsonify(failure)
    finally:
        conn.close()

@app.route('/dataset/<var>', methods=['GET'])
def dsdisplay(var):
    """To be fetch data without filters"""
    conn = sq.connect(**config)
    cur = conn.cursor()
    try:
        query = "SELECT ViewName FROM MasterTable WHERE DisplayName LIKE '" + var + "'"
        cur.execute(query)
        rows = cur.fetchall()
        
        if cur.with_rows == True and cur.rowcount > 0:
            for row in rows:
                print(row)

            query = "SELECT * FROM " + str(row[0])
            
            cur.execute(query)
            rows = cur.fetchall()

            items = []

            items = [dict(zip([key[0] for key in cur.description],row)) for row in rows]

            response = app.response_class(
                response=json.dumps(items, default=json_serial),
                status=200,
                mimetype='application/json')

            return response
        failure['error message'] = "Required data not yet configured"
        return jsonify(failure)
    except Exception as e:
        failure['error message'] = str(e)
        return jsonify(failure)
    finally:
        conn.close()

@app.route('/dataset/<var>/filter', methods=['GET'])
def dsdisplayfilter(var):
    """To be fetch data with filters"""
    conn = sq.connect(**config)
    cur = conn.cursor()
    try:
        query = "SELECT ViewName, KeyName FROM MasterTable WHERE DisplayName LIKE '" + var + "'"
        cur.execute(query)
        rows = cur.fetchall()
        
        if cur.with_rows == True and cur.rowcount > 0:
            for row in rows:
                tablenm = row[0]
                columnm = row[1]

            query_parameters = request.args

            Key = '' if query_parameters.get(columnm) is None else " AND " + columnm + " LIKE '" + query_parameters.get(columnm) + "'"
            ProjectCode = '' if query_parameters.get('ProjectCode') is None else " AND ProjectCode LIKE '" + query_parameters.get('ProjectCode') + "'"

            if Key == '' and ProjectCode == '':
                Key = " AND 0=1"

            query = "SELECT * FROM " + tablenm + " WHERE 1=1" + Key + ProjectCode
            cur.execute(query)
            rows = cur.fetchall()
            items = []

            items = [dict(zip([key[0] for key in cur.description],row)) for row in rows]

            response = app.response_class(
                response=json.dumps(items, default=json_serial),
                status=200,
                mimetype='application/json')

            return response
        failure['error message'] = "Required data not yet configured"
        return jsonify(failure)
    except Exception as e:
        failure['error message'] = str(e)
        return jsonify(failure)
    finally:
        conn.close()

@app.route('/dataset/<var>', methods=['POST'])
def dsinsert(var):
    """To be store data without filters"""
    conn = sq.connect(**config)
    cur = conn.cursor()
    try:
        query = "SELECT TableName FROM MasterTable WHERE DisplayName LIKE '" + var + "'"
        cur.execute(query)
        rows = cur.fetchall()
        
        if cur.with_rows == True and cur.rowcount > 0:
            for row in rows:
                print(row)

            dt = {}
            record_list = request.get_json()

            query = "INSERT INTO {}".format(str(row[0]))

            # if record list then get column names from first key
            if type(record_list) == list:
                first_record = record_list[0]
                columns = list(first_record.keys())

            # if just one dict obj or nested JSON dict
            else:
                print ("Needs to be an array of JSON objects")

            # enclose the column names within parenthesis
            query += "(" + ', '.join(columns) + ")\nVALUES "

            # enumerate over the record
            for i, record_dict in enumerate(record_list):

                # iterate over the values of each record dict object
                values = []
                for col_names, val in record_dict.items():

                    # Postgres strings must be enclosed with single quotes
                    if type(val) == str:
                        # escape apostrophies with two single quotations
                        val = val.replace("'", "''")
                        val = "'" + val + "'"
                        values += [ str(val) ]

                # join the list of values and enclose record in parenthesis
                query += "(" + ', '.join(values) + "),\n"

            # remove the last comma and end statement with a semicolon
            query = query[:-2] + ";"
            query = query.replace("''","NULL")# replace empty with NULL value
            #print(query)
            cur.execute(query)

            conn.commit()

            return jsonify(success)
        failure['error message'] = "Required data not yet configured"
        return jsonify(failure)
    except Exception as e:
        failure['error message'] = str(e)
        return jsonify(failure)
    finally:
        conn.close()

@app.route('/dataset/<var>', methods=['PUT'])
def dsupdate(var):
    """To be change data without filters"""
    conn = sq.connect(**config)
    cur = conn.cursor()
    try:
        query = "SELECT TableName, KeyName FROM MasterTable WHERE DisplayName LIKE '" + var + "'"
        rows = cur.fetchall()
        
        if cur.with_rows == True and cur.rowcount > 0:
            rows = cur.fetchall()
            for row in rows:
                tablenm = row[0]
                columnm = row[1]

            lsupdt = []
            dt = {}
            record_list = request.get_json()

            query = "INSERT INTO {}".format(str(tablenm))

            # if record list then get column names from first key
            if type(record_list) == list:
                first_record = record_list[0]
                columns = list(first_record.keys())

            # if just one dict obj or nested JSON dict
            else:
                print ("Needs to be an array of JSON objects")

            # enclose the column names within parenthesis
            query += "(" + ', '.join(columns) + ")\nVALUES "

            # enumerate over the record
            for i, record_dict in enumerate(record_list):

                # iterate over the values of each record dict object
                values = []
                for col_names, val in record_dict.items():

                    # Postgres strings must be enclosed with single quotes
                    if type(val) == str:
                        # escape apostrophies with two single quotations
                        val = val.replace("'", "''")
                        if col_names == columnm:
                            q = "UPDATE " + tablenm + " SET RowStatus = 0 WHERE " + columnm + " = '" + val + "' AND RowStatus = 1"
                            cur.execute(q)
                        val = "'" + val + "'"
                        values += [ str(val) ]

                # join the list of values and enclose record in parenthesis
                query += "(" + ', '.join(values) + "),\n"

            # remove the last comma and end statement with a semicolon
            query = query[:-2] + ";"
            query = query.replace("''","NULL")# replace empty with NULL value
            #print(query)
            cur.execute(query)

            conn.commit()

            return jsonify(success)
        failure['error message'] = "Required data not yet configured"
        return jsonify(failure)
    except Exception as e:
        failure['error message'] = str(e)
        return jsonify(failure)
    finally:
        conn.close()

@app.route('/dataset/<var>', methods=['DELETE'])
def dsdelete(var):
    """To be remove data without filters"""
    conn = sq.connect(**config)
    cur = conn.cursor()
    try:
        query = "SELECT TableName, KeyName FROM MasterTable WHERE DisplayName LIKE '" + var + "'"
        cur.execute(query)
        rows = cur.fetchall()
        
        if cur.with_rows == True and cur.rowcount > 0:
            for row in rows:
                tablenm = row[0]
                columnm = row[1]

            record_list = request.get_json()
            for row in record_list:
                for Key, Value in row.items():
                    if Key == columnm:
                        query = "UPDATE " + tablenm + " SET RowStatus = 0 WHERE RowStatus = 1 AND " + Key + " LIKE '"+ str(Value) + "'"
                        cur.execute(query)
            conn.commit()

            return jsonify(success)
        failure['error message'] = "Required data not yet configured"
        return jsonify(failure)
    except Exception as e:
        failure['error message'] = str(e)
        return jsonify(failure)
    finally:
        conn.close()

@app.route('/dataset/<var>/filter', methods=['DELETE'])
def dsdeletefilter(var):
    """To be fetch data with filters"""
    conn = sq.connect(**config)
    cur = conn.cursor()
    try:
        query = "SELECT TableName, KeyName FROM MasterTable WHERE DisplayName LIKE '" + var + "'"
        cur.execute(query)
        rows = cur.fetchall()
        
        if cur.with_rows == True and cur.rowcount > 0:
            for row in rows:
                tablenm = row[0]
                columnm = row[1]

            query_parameters = request.args

            Key = '' if query_parameters.get(columnm) is None else " AND " + columnm + " LIKE '" + query_parameters.get(columnm) + "'"
            ProjectCode = '' if query_parameters.get('ProjectCode') is None else " AND ProjectCode LIKE '" + query_parameters.get('ProjectCode') + "'"

            if Key == '' and ProjectCode == '':
                Key = " AND 0=1"

            query = "UPDATE " + tablenm + " SET RowStatus = 0 WHERE RowStatus = 1" + Key + ProjectCode
            cur.execute(query)

            conn.commit()

            return jsonify(success)
        failure['error message'] = "Required data not yet configured"
        return jsonify(failure)
    except Exception as e:
        failure['error message'] = str(e)
        return jsonify(failure)
    finally:
        conn.close()

#Response for not handles routes
@app.errorhandler(404)
def page_not_found(e):
    """Response for not handles routes"""
    return "<h1>404</h1><p>The resource could not be found.</p>",404

if __name__ == '__main__':
    app.run(host='0.0.0.0',port=80,debug=True)