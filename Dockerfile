FROM python:3.7-slim
ADD app.py /
ADD requirements.txt /
RUN pip install -r requirements.txt
RUN pip install Flask gunicorn
EXPOSE 80
CMD exec gunicorn --bind :$PORT --workers 1 --theards 8 --timeout 0 app:app